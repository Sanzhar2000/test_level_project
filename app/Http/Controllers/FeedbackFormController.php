<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackFormController extends Controller
{
    public function submit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|string',
            'message' => 'required',
        ]);
        //if validation fails it automatically returns a 422 response

        $feedback = new Feedback();
        $feedback->name = $request->name;
        $feedback->phone = $request->phone;
        $feedback->message = $request->message;
        $feedback->save();

        return response()->json(['status'=>true, 'data'=>'Feedback submitted successfully.'], 200);
    }
}
